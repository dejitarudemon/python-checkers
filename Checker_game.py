"""
Game engine
author: @yura555 (gitlab)
        @dejitarudemon (gitlab)
19.11.21
20.11.21
-- Fuck it, let's try again
"""

import pygame

from tkinter import *
from objects.checker import Checker
from objects.rules.russian import RussianRules
from objects.rules.england import EnglandRules
from game_functions import *
from event_handling_functions import *
from objects.player import Player
from config_game import *
from config import Config

root = Tk()
root.title("Menu")
root.geometry("300x300")
config = Config(root)

root.mainloop()

if config.data.get("game_mode") == "ru":
    rule = RussianRules("Russian")
else:
    rule = EnglandRules("England")

players = [Player(config.data.get("name_1"), 'white', WIDTH), Player(config.data.get("name_2"), "black", WIDTH)]
count_num, count_player = get_start_player(players, rule)

running = True

while running:
    # Держим цикл на правильной скорости
    clock.tick(FPS)
    screen.blit(board_img, board_img_place)
    # Ввод процесса (события)

    if is_updated_checkers is False:
        available_moves = update_sprites(all_checkers, count_player, rule, [WIDTH, HEIGHT, available_move_img])

    for event in pygame.event.get():
        # check for closing window

        if event.type == pygame.QUIT:
            running = False
            break

        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1 and is_clicked is False:
            checker, is_clicked = click_handling(all_checkers, count_player)

        if event.type == pygame.MOUSEBUTTONUP and event.button == 1 and is_clicked is True:
            new_position = get_position()
            new_checker = find_checker(all_checkers, new_position)
            check_move = rule.check_move(checker, all_checkers, new_position)
            check_eat = rule.check_eat(checker, all_checkers, new_position)

            if check_available(new_position, available_moves) is True:

                if check_move is True:
                    checker.update(new_position)
                    count_num, count_player = swap(count_num, count_player, players)

                elif check_eat is True:
                    position_of_eating_checker = rule.eating_position

                    all_checkers.remove(find_checker(all_checkers, position_of_eating_checker))
                    checker.update(new_position)

                    available_moves = update_sprites(all_checkers, count_player, rule,
                                                     [WIDTH, HEIGHT, available_move_img])

                    count_num, count_player = check_continue(available_moves, count_num,
                                                             count_player, players, new_position)

            transform(checker, white_check_king_img, black_check_king_img, HEIGHT)
            is_clicked = False

    all_checkers.draw(screen)
    available_moves.draw(screen)

    # screen.blit(board_img, board_img_place)

    # После отрисовки всего, переворачиваем экран
    pygame.display.flip()

pygame.quit()
