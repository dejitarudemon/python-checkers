from tkinter import *
import json
import sys
import os


class Config(Frame):


    def __init__(self, master):
        self.master = master
        super(Config, self).__init__(master)
        self.grid()

        self.file_path = os.path.dirname(__file__) + "/config/config.json"
        self.load_config()

        self.create_widgets()

        self.MENU_WIDGETS = {self.lbl_game_name, self.bttn_start, self.bttn_config, self.bttn_exit}
        self.CONFIG_WIDGETS = {self.lbl_name_1, self.lbl_name_2, self.ent_name_1, self.ent_name_2, self.lbl_mode,
                               self.bttn_save, self.radiobttn1, self.radiobttn2, self.bttn_cancel, self.count_settings}

    def create_widgets(self):
        self.lbl_game_name = Label(self, text="C H E C K E R S")
        self.lbl_game_name.grid(row=0, column=1, sticky="W")

        self.bttn_start = Button(self)
        self.bttn_start["text"] = "Start game"
        self.bttn_start["command"] = self.master.destroy
        self.bttn_start.grid(row=1, column=0, sticky='W')

        self.bttn_config = Button(self)
        self.bttn_config["text"] = "Configurations"
        self.bttn_config["command"] = self.transform_to_config
        self.bttn_config.grid(row=1, column=1, sticky='W')

        self.bttn_exit = Button(self)
        self.bttn_exit["text"] = "Exit"
        self.bttn_exit["command"] = lambda: sys.exit()
        self.bttn_exit.grid(row=1, column=2, sticky='W')

        self.lbl_name_1 = Label(self, text="first player")
        self.lbl_name_1.grid_forget()

        self.lbl_name_2 = Label(self, text="second player")
        self.lbl_name_2.grid_forget()

        self.ent_name_1 = Entry(self)
        self.ent_name_1.grid_forget()

        self.ent_name_2 = Entry(self)
        self.ent_name_2.grid_forget()

        self.lbl_mode = Label(self, text="Game mode:")
        self.lbl_mode.grid_forget()

        self.game_mode = StringVar()
        self.game_mode.set(None)

        self.radiobttn1 = Radiobutton(self,
                    text="Russian",
                    variable=self.game_mode,
                    value="ru")

        self.radiobttn1.grid_forget()

        self.radiobttn2 = Radiobutton(self,
                    text="England",
                    variable=self.game_mode,
                    value="en")

        self.radiobttn2.grid_forget()

        self.bttn_save = Button(self)
        self.bttn_save["text"] = "Save config"
        self.bttn_save["command"] = self.save_configs
        self.bttn_save.grid_forget()

        self.bttn_cancel = Button(self)
        self.bttn_cancel["text"] = "Return"
        self.bttn_cancel["command"] = self.transform_to_menu
        self.bttn_cancel.grid_forget()

        self.count_settings = Text(self, width=25, height=4, wrap=WORD)
        self.count_settings.grid_forget()

        self.count_settings.delete(0.0, END)
        self.count_settings.insert(0.0, json.dumps(self.data))

    def save_configs(self):

        if self.ent_name_1.get() == '':
            name1 = self.data.get('name_1')
        else:
            name1 = self.ent_name_1.get()

        if self.ent_name_2.get() == '':
            name2 = self.data.get('name_2')
        else:
            name2 = self.ent_name_2.get()

        if self.game_mode.get() == 'None':
            game_mode = self.data.get('game_mode')
        else:
            game_mode = self.game_mode.get()

        self.data = {
            "name_1": name1,
            "name_2": name2,
            "game_mode": game_mode
        }
        try:
            with open(self.file_path, "w") as config_file:
                json.dump(self.data, config_file)
        except FileNotFoundError:
            self.create_config()

        self.count_settings.delete(0.0, END)
        self.count_settings.insert(0.0, json.dumps(self.data))

    def create_config(self):
        """
        Function to create config file

        Returns: none

        """
        self.data = {
            "name_1": None,
            "name_2": None,
            "game_mode": "Russian"
        }
        try:
            with open(self.file_path, "w+") as config_file:
                json.dump(self.data, config_file)
        except IsADirectoryError:
            return
        except AssertionError:
            return

    def load_config(self):
        try:
            with open(self.file_path, "r") as config_file:
                self.data = json.load(config_file)

        except FileNotFoundError:
            self.create_config()

    def transform_to_config(self):
        for widget in self.MENU_WIDGETS:
            widget.grid_forget()

        self.lbl_name_1.grid(row=0, column=0, columnspan=1, sticky='W')
        self.lbl_name_2.grid(row=3, column=0, sticky='W')
        self.ent_name_1.grid(row=1, column=0, sticky='W')
        self.ent_name_2.grid(row=4, column=0, sticky='W')
        self.lbl_mode.grid(row=5, column=0, sticky='W')
        self.radiobttn1.grid(row=6, column=0, sticky="W")
        self.radiobttn2.grid(row=7, column=0, sticky="W")
        self.bttn_save.grid(row=8, column=0, sticky='W')
        self.bttn_cancel.grid(row=8, column=1, sticky='E')
        self.count_settings.grid(row=9, column=0, sticky='W')

    def transform_to_menu(self):
        for widget in self.CONFIG_WIDGETS:
            widget.grid_forget()

        self.lbl_game_name.grid(row=0, column=1, sticky="W")
        self.bttn_start.grid(row=1, column=0, sticky='W')
        self.bttn_config.grid(row=1, column=1, sticky='W')
        self.bttn_exit.grid(row=1, column=2, sticky='W')