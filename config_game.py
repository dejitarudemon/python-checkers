import os
import pygame
from game_functions import *


WIDTH = 800
HEIGHT = 800
FPS = 10

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)

pygame.init()
pygame.mixer.init()
pygame.display.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Checkers")
clock = pygame.time.Clock()

game_folder = os.path.dirname(__file__)
img_folder = os.path.join(game_folder, 'objects/images')

black_check_not_img = pygame.image.load(os.path.join(img_folder, 'black_checker_not_king.png')).convert()
white_check_not_img = pygame.image.load(os.path.join(img_folder, 'white_checker_not_king.png')).convert()
black_check_king_img = pygame.image.load(os.path.join(img_folder, 'black_checker_king.png')).convert()
white_check_king_img = pygame.image.load(os.path.join(img_folder, 'white_checker_king.png')).convert()
available_move_img = pygame.image.load(os.path.join(img_folder, 'move.png')).convert()
board_img = pygame.image.load(os.path.join(img_folder, "board.jpg"))
board_img_place = (0, 0, 1000, 1000)


all_checkers = start_checkers(HEIGHT, WIDTH, black_check_not_img, white_check_not_img)

mouse_x = -1
mouse_y = -1
is_clicked = False
is_updated_checkers = False
checker = None


available_moves = pygame.sprite.Group()