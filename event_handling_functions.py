import pygame
from game_functions import *


def get_position():
    position = [pygame.mouse.get_pos()[0] // 100 * 100 + 50,
                pygame.mouse.get_pos()[1] // 100 * 100 + 50]
    return position


def click_handling(all_checkers, count_player):
    position = get_position()
    checker = find_checker(all_checkers, position)
    is_clicked = False
    if checker is not None:
        if checker.color == count_player.color:
            is_clicked = True
    return checker, is_clicked


def update_sprites(all_checkers, count_player, rule, screen_param):

    movies = pygame.sprite.Group()
    for another_checker in all_checkers:
        if another_checker.color == count_player.color:
            movies.add(update_available_moves(
                rule.find_available_moves(another_checker, all_checkers,
                                          screen_param[0], screen_param[1]), screen_param[2]))

    check_force_moving(movies)
    return movies


def check_continue(available_moves, count_num, count_player, players, new_position):
    for move in available_moves:
        if move.color == 'eat' and move.position[0] != new_position[0] and move.position[1] != new_position[1]:
            return count_num, count_player

    count_num = (count_num + 1) % 2
    count_player = players[count_num]

    return count_num, count_player


def swap(count_num, count_player, players):

    count_num = (count_num + 1) % 2
    count_player = players[count_num]

    return count_num, count_player

def transform(checker, white_check_king_img, black_check_king_img, heigth):
    if check_transformation(checker, heigth) is True:
        if checker.color == 'white':
            image = white_check_king_img
        else:
            image = black_check_king_img

        checker.transformation(image)

