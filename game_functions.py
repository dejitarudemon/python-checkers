import pygame
from objects.checker import *


def start_checkers(height, width, black_check_not_img, white_check_not_img):
    result = pygame.sprite.Group()
    for i in range(0, width // 100, 2):
        for j in range(0, height // 100):
            if j < width // 200 - 1:
                image = black_check_not_img
                color = 'black'
            elif width // 200 < j:
                image = white_check_not_img
                color = 'white'
            else:
                image = None
                color = None

            if image is not None:
                if j % 2 != 0:
                    result.add(Checker([(i + 1) * 100 + 50, j * 100 + 50], color, image))
                else:
                    result.add(Checker([i * 100 + 50, j * 100 + 50], color, image))
    return result


def find_checker(checkers, positions):
    for one_checker in checkers:
        if one_checker.check(positions) is True:
            return one_checker

    return None


def get_start_player(players, rule):
    if players[0].color == rule.start:
        count_player = players[0]
        count_num = 0
    else:
        count_player = players[1]
        count_num = 1
    return count_num, count_player


def check_transformation(one_checker, height):
    color = one_checker.color

    if color == "white" and one_checker.position[1] == 50:
        return True

    elif color == "black" and one_checker.position[1] == height - 50:
        return True

    return False


def update_available_moves(movies, image):
    result = pygame.sprite.Group()
    for move in movies:
        result.add(Checker([move[0], move[1]], move[2], image))

    return result


def check_available(position, available_movies):
    for move in available_movies:
        if position[0] == move.position[0] and position[1] == move.position[1]:
            return True

    return False


def check_force_moving(movies):
    is_eating_checker = False
    for move in movies:
        if move.color == 'eat':
            is_eating_checker = True
            break

    if is_eating_checker is True:
        for move in movies:
            if move.color == 'move':
                movies.remove(move)
