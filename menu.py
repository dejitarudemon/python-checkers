from tkinter import *
from config.config import start_config_menu
import json
import sys
import os


class Menu(Frame):
    def __init__(self, master):
        super(Menu, self).__init__(master)
        self.grid()
        self.create_widgets()

    def create_widgets(self):
        self.bttn_game = Button(self)
        self.bttn_game["text"] = "Start game"
        #self.bttn_game["command"] = self.start
        self.bttn_game.grid(row=0, column=1, sticky='W')

        self.bttn_settings = Button(self)
        self.bttn_settings["text"] = "Settings"
        self.bttn_settings["command"] = start_config_menu
        self.bttn_settings.grid(row=1, column=1, sticky='W')

        self.bttn_exit = Button(self)
        self.bttn_exit["text"] = "Exit"
        self.bttn_exit["command"] = self.exit_game
        self.bttn_exit.grid(row=2, column=1, sticky='W')

    def exit_game(self):
        sys.exit()


root = Tk()
root.title("Game menu")
root.geometry("300x300")

config = Menu(root)

root.mainloop()
