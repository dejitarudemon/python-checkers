"""
Game's object Checker
authors: @dejitarudemon (gitlab)
         @yura555 (gitlab)
date: 19.11.21
"""
import pygame


class Checker(pygame.sprite.Sprite):
    """
        class Checker contains 6 parameters:
            pos_x - horizontal position
            pos_y - vertical position
            color - color of checker
            is_king - label indicating that the checker is a king
            image - sprite
            rect - hitbox
        """

    def __init__(self, position, color, image, is_king=False):
        pygame.sprite.Sprite.__init__(self)
        self.position = position
        self.color = color
        self.is_king = is_king

        self.image = image
        self.image.set_colorkey((255, 255, 255))

        self.rect = self.image.get_rect()
        self.rect.center = position

    def update(self, new_position):
        """
        Moving the checker to another position
        Args:
            new_position - new position odf checker

        Returns: none

        """
        self.position = new_position

        pygame.sprite.Sprite.update(self)
        self.rect.center = self.position

    def transformation(self, image):
        """
        Checker's transformation to a stain
        Returns:none

        """
        self.is_king = True
        self.image = image
        self.image.set_colorkey((255, 255, 255))

        self.update(self.position)

    def __str__(self):
        pygame.sprite.Sprite.__str__(self)
        return str([self.pos_x, self.pos_y])

    def __getitem__(self, item):
        if item == 'x':
            return self.pos_x
        elif item == 'y':
            return self.pos_y
        elif item == 'is_king':
            return self.is_king
        elif item == 'color':
            return self.color
        else:
            return None

    def __repr__(self):
        return self.color + str(self.pos_x) + str(self.pos_y)

    def check(self, position):
        return self.position[0] == position[0] and self.position[1] == position[1]

    def get_pos(self):
        return self.position
