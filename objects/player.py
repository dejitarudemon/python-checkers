"""
Game's object Player
author: @dejitarudemon (gitlab)
date: 19.11.21
"""


class Player:
    """
    class Player contains 2 parameters:
        size - size of game's field
        field - list with lists which contain checkers
    """

    def __init__(self, name, color, width):
        self.name = name
        self.color = color
        if width == 800:
            checkers_ratio = 1
        else:
            checkers_ratio = 0

        self.number_of_checkers = width // 100 * (width // 100 - 2) / 4 - checkers_ratio
