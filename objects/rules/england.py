from objects.checker import *
from game_functions import *


class EnglandRules:
    def __init__(self, name):
        self.name = name
        self.eating_position = []
        self.start = "black"

    def check_move(self, first_checker, all_checkers, position):
        if first_checker is None or find_checker(all_checkers, position) is not None:
            return False

        result = False

        if first_checker.is_king is False:
            if first_checker.color == 'white':
                result = -101 < position[1] - first_checker.position[1] < 0 \
                         and -101 < position[0] - first_checker.position[0] < 101 \
                         and position[0] - first_checker.position[0] != 0

            else:
                result = 0 < position[1] - first_checker.position[1] < 101 \
                         and 0 != position[0] - first_checker.position[0] \
                         and -101 < position[0] - first_checker.position[0] < 101

        else:
            result = -101 < position[1] - first_checker.position[1] < 101 \
                     and -101 < position[0] - first_checker.position[0] < 101 \
                     and position[0] - first_checker.position[0] != 0 \
                     and position[1] - first_checker.position[1] != 0

        return result

    def check_eat(self, first_checker, all_checkers, position):
        if first_checker is None or find_checker(all_checkers, position) is not None:
            return False

        result = False

        self.eating_position = [(position[0] + first_checker.position[0]) // 2,
                                (position[1] + first_checker.position[1]) // 2]

        if find_checker(all_checkers, self.eating_position) is None:
            return False

        if first_checker.color == find_checker(all_checkers, self.eating_position).color:
            return False

        if first_checker.is_king is False:

            if first_checker.color == 'white':
                result = -201 < position[1] - first_checker.position[1] < 0 \
                         and -201 < position[0] - first_checker.position[0] < 201 \
                         and position[0] - first_checker.position[0] != 0

            else:
                result = 0 < position[1] - first_checker.position[1] < 201 \
                         and 0 != position[0] - first_checker.position[0] \
                         and -201 < position[0] - first_checker.position[0] < 201

        else:
            if position[1] - first_checker.position[1] != 0 \
                    and position[0] - first_checker.position[0] != 0 \
                    and position[1] - first_checker.position[1] == position[0] - first_checker.position[0] \
                    or position[1] - first_checker.position[1] == -(position[0] - first_checker.position[0]):

                result = -201 < position[1] - first_checker.position[1] < 201 \
                         and -201 < position[0] - first_checker.position[0] < 201 \
                         and position[0] - first_checker.position[0] != 0

        return result

    def find_available_moves(self, one_checker, checkers, width, height):
        if one_checker is None:
            return None

        result = []

        for i in range(50, width, 100):
            for j in range(50, height, 100):
                if self.check_move(one_checker, checkers, [i, j]) is True:
                    result.append([i, j, 'move'])
                elif self.check_eat(one_checker, checkers, [i, j]) is True:
                    result.append([i, j, 'eat'])

        return result

