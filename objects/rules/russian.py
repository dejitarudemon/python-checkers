from objects.checker import *
from game_functions import *


class RussianRules:
    def __init__(self, name):
        self.name = name
        self.eating_position = []
        self.start = "white"

    def check_move(self, first_checker, all_checkers, position):
        if first_checker is None or find_checker(all_checkers, position) is not None:
            return False

        result = False

        if first_checker.is_king is False:
            if first_checker.color == 'white':
                result = -101 < position[1] - first_checker.position[1] < 0 \
                         and -101 < position[0] - first_checker.position[0] < 101 \
                         and position[0] - first_checker.position[0] != 0

            else:
                result = 0 < position[1] - first_checker.position[1] < 101 \
                         and 0 != position[0] - first_checker.position[0] \
                         and -101 < position[0] - first_checker.position[0] < 101

        else:
            if position[1] - first_checker.position[1] != 0 \
                    and position[0] - first_checker.position[0] != 0 \
                    and position[1] - first_checker.position[1] == position[0] - first_checker.position[0] \
                    or position[1] - first_checker.position[1] == -(position[0] - first_checker.position[0]):

                if first_checker.position[1] - position[1] > 0:
                    cof_y = 100
                else:
                    cof_y = -100

                if first_checker.position[0] - position[0] > 0:
                    cof_x = 100
                else:
                    cof_x = -100

                j = position[1] - cof_y

                for i in range(position[0], first_checker.position[0], cof_x):
                    j += cof_y
                    if find_checker(all_checkers, [i, j]) is not None:
                        return False

                result = True

        return result

    def check_eat(self, first_checker, all_checkers, position):
        if first_checker is None or find_checker(all_checkers, position) is not None:
            return False

        result = False

        if first_checker.is_king is False:
            self.eating_position = [(position[0] + first_checker.position[0]) // 2,
                                    (position[1] + first_checker.position[1]) // 2]

            if find_checker(all_checkers, self.eating_position) is None:
                return False

            if first_checker.color == find_checker(all_checkers, self.eating_position).color:
                return False

            result = -201 < position[1] - first_checker.position[1] < 201 \
                     and -201 < position[0] - first_checker.position[0] < 201 \
                     and position[0] - first_checker.position[0] != 0
        else:
            if position[1] - first_checker.position[1] != 0 \
                    and position[0] - first_checker.position[0] != 0 \
                    and position[1] - first_checker.position[1] == position[0] - first_checker.position[0] \
                    or position[1] - first_checker.position[1] == -(position[0] - first_checker.position[0]):

                if first_checker.position[1] - position[1] > 0:
                    cof_y = 100
                else:
                    cof_y = -100

                if first_checker.position[0] - position[0] > 0:
                    cof_x = 100
                else:
                    cof_x = -100

                j = position[1] - cof_y
                flip_flag = False

                for i in range(position[0], first_checker.position[0], cof_x):
                    j += cof_y
                    if find_checker(all_checkers, [i, j]) is not None and flip_flag is True:
                        return False
                    elif find_checker(all_checkers, [i, j]) is not None and flip_flag is False:
                        if first_checker.color == find_checker(all_checkers, [i, j]).color:
                            return False
                        self.eating_position = [i, j]
                        flip_flag = True

                result = True

        return result

    def find_available_moves(self, one_checker, checkers, width, height):
        if one_checker is None:
            return None

        result = []

        for i in range(50, width, 100):
            for j in range(50, height, 100):
                if self.check_move(one_checker, checkers, [i, j]) is True:
                    result.append([i, j, 'move'])
                elif self.check_eat(one_checker, checkers, [i, j]) is True:
                    result.append([i, j, 'eat'])

        return result
